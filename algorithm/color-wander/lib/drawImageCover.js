// fits the PIXI.Sprite to the parent
// similar to CSS background-size: cover
module.exports = drawImageCover;
function drawImageCover (ctx, image, parent, scale, rotation) {
  scale = typeof scale === 'number' ? scale : 1;
  rotation = typeof rotation === 'number' ? rotation : 0;
  parent = typeof parent === 'undefined' ? ctx.canvas : parent;

  var tAspect = image.width / image.height;
  var pWidth = parent.width;
  var pHeight = parent.height;

  var pAspect = pWidth / pHeight;

  var width, height;
  if (tAspect > pAspect) {
    height = pHeight;
    width = height * tAspect;
  } else {
    width = pWidth;
    height = width / tAspect;
  }
  width *= scale;
  height *= scale;
  var x = (pWidth - width) / 2;
  var y = (pHeight - height) / 2;

  // Save Transformation Matrix
  ctx.save();
  // Set origin to center
  ctx.translate(pWidth/2, pHeight/2);
  // Do the rotation
  ctx.rotate(rotation*Math.PI/180)
  // Set origin back to upper left
  ctx.translate(-pWidth/2, -pHeight/2);
  // Draw the image
  ctx.drawImage(image, x, y, width, height);
  // Restore original Transformation Matrix
  ctx.restore();
}
