document.addEventListener("DOMContentLoaded", function() {
  var button = jQuery("#color-wander_submit");
  button.on("click", submitColorWander);
});

function submitColorWander(){
  console.log("Submitting:");
  var imgBase64 = canvas.toDataURL('image/png');
  var seed = jQuery(".seed-text").text();
  console.log(`Seed: ${seed}`);
  console.log(`Filesize: ${Math.round((imgBase64.length - 22)*3/4)}`);
  jQuery.ajax({
    method: "POST",
    url: "/algorithm/php/controller.php",
    dataType: 'json',
    data: {
       type: "color-wander",
       imgBase64: imgBase64,
       seed: seed
    }
  }).done(function(o) {
    console.log(o);
    window.location.href = o.permalink;
  }).fail(function(o) {
    console.log(o.responseText);
  });
}

