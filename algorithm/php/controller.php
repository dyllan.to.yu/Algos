<?php
include('debug.php');
require_once('imager.php');
require_once('class/ColorWander.php');

try{
  $res = [];
  switch ($_POST['type']) {
    case 'color-wander':
    	$item = new ColorWander($_POST['imgBase64'], $_POST['seed'], 'img/color_wander/');
    	$item->create();
    	$res['permalink'] = $item->permalink;
      break;
    
    default:
      throw new Exception("ERROR: INVALID TYPE", 1);
      break;
  }
  echo json_encode($res);
} catch (Exception $e){
  echo $e->getMessage();
}