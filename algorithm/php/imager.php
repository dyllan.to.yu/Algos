<?php
function saveImage($img, $name, $dir = 'img/'){
	$nDir = __dir__ . '/../' . $dir;
  $name .= '.png';
  $img = str_replace('data:image/png;base64', '', $img);
  $img = str_replace(' ', '+', $img);
  $fdata = base64_decode($img);
  $fname = $nDir.$name;
  $success = file_put_contents($fname,$fdata);
  if (!$success) throw new Exception("Error Saving Image");
  $image = removeBG_PNG($dir, $name);
  $image['original'] = $dir.$name;
  return $image;
}

// Please include trailing slash in dir
function removeBG_PNG($dir, $file){
	$nDir = __dir__ . '/../' . $dir;
	$picture = imagecreatefrompng($nDir.$file);

	$img_w = imagesx($picture);
	$img_h = imagesy($picture);

	$newPicture = imagecreatetruecolor( $img_w, $img_h );
	imagesavealpha( $newPicture, true );
	$rgb = imagecolorallocatealpha( $newPicture, 0, 0, 0, 127 );
	imagefill( $newPicture, 0, 0, $rgb );

	$color = imagecolorat( $picture, $img_w-1, 1);

	for( $x = 0; $x < $img_w; $x++ ) {
	    for( $y = 0; $y < $img_h; $y++ ) {
	        $c = imagecolorat( $picture, $x, $y );
	        if($color!=$c){         
	            imagesetpixel( $newPicture, $x, $y,    $c);             
	        }           
	    }
	}
	$name_comp = explode(".", $file);
	if(count($name_comp) != 2) throw new Exception("More than one '.' in filename", 1);
	
	$fname = $name_comp[0].'t.'.$name_comp[1];
	imagepng($newPicture, $nDir.$fname);
	imagedestroy($newPicture);
	imagedestroy($picture);

	$hex_color = colorToHex($color);

	return ['transparent'=>$dir.$fname, 'color'=>$hex_color];
}

function colorToHex($c){
	$r = ($c >> 16) & 0xFF;
	$g = ($c >> 8) & 0xFF;
	$b = $c & 0xFF;

	return sprintf("#%02x%02x%02x", $r, $g, $b);
}