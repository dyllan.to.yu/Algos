<?php

function getDesignID($file){
  $url = "https://thealgos.design/algorithm/".$file;
  $ep = "design";
  $data = [
    'type' => 'dtg',
    'sides' => [
      'front' => [
        'artwork' => $url,
        'dimensions' => [
          'width' => 14
        ],
        'position' => [
          'horizontal' => 'C',
          'offset' => [
            'top' => 2
          ]
        ]
      ]
    ]/*,
    'customization' => [
      ['id' => $customization_id]
    ]*/
  ];

  return scalablePressRequest($ep, $data)['designId'];
}

function scalablePressRequest($ep, $data){
  $auth = ":live_hPh3gPG4HheYA7FfORHXIA";
  $test_auth = ":test_02IkoZ-uq1_vs-MW3LrAgg";


  $url = "https://api.scalablepress.com/v2/".$ep;
  $headers = array(
    'Content-type: application/json'
  );

  $data = json_encode($data);

  $ch = curl_init();
  $options = [
    CURLOPT_URL => $url,
    CURLOPT_USERPWD => $auth,
    CURLOPT_HTTPHEADER => $headers,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_POSTFIELDS => $data
  ];
  curl_setopt_array($ch, $options);

  $result = curl_exec($ch);
  curl_close($ch);
  return json_decode($result, true);
}