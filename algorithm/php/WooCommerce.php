<?php
require __DIR__ . '/vendor/autoload.php';

use Automattic\WooCommerce\Client;

class WooCommerce{
	var $woocommerce;
	function __construct(){
		$this->woocommerce = new Client(
			'https://thealgos.design',
			'ck_5036c204f64edcd0bd9ddf59331d4ce089daadfd',
			'cs_cc8a6e5e80c98148bc28579c7c5aff6c53d08be5',
			[
				'wp_api' => true,
				'version' => 'wc/v2'
			]
		);
	}

	function createProduct($data){
		return $this->woocommerce->post('products', $data);
	}

	function createVariations($id, $data){
		$ep = "products/$id/variations/batch";
		return $this->woocommerce->post($ep, $data);
	}
}
