<?php
include('debug.php');
include('bgremove.php');

try{
  $res = [];
  switch ($_POST['type']) {
    case 'color-wander':
      $img = saveImage($_POST['imgBase64'], $_POST['seed'], 'img/color_wander/');
      $res['permalink'] = addProduct(colorWanderItem($_POST['seed']));
      $res['designID'] = scalablePressDesignID($img['transparent']);
      break;
    
    default:
      echo "ERROR: INVALID TYPE";
      break;
  }
  echo json_encode($res);
} catch (Exception $e){
  echo $e->getMessage();
}

function saveImage($img, $name, $dir = 'img/'){
  $name .= '.png';
  $img = str_replace('data:image/png;base64', '', $img);
  $img = str_replace(' ', '+', $img);
  $fdata = base64_decode($img);
  $fname = $dir.$name;
  $success = file_put_contents($dir.$name,$fdata);
  if (!$success) throw new Exception("Error Saving Image");
  $image = removeBG_PNG($dir, $name);
  $image['original'] = $dir.$name;
  return $image;
}

function scalablePressDesignID($file){
  $url = "https://thealgos.design/algorithm/".$file;
  $ep = "design";
  $data = [
    'type' => 'dtg',
    'sides' => [
      'front' => [
        'artwork' => $url,
        'dimensions' => [
          'width' => 14
        ],
        'position' => [
          'horizontal' => 'C',
          'offset' => [
            'top' => 1.5
          ]
        ]
      ]
    ]/*,
    'customization' => [
      ['id' => $customization_id]
    ]*/
  ];

  return scalablePressRequest($ep, $data);
}

function scalablePressRequest($ep, $data){
  $auth = ":live_hPh3gPG4HheYA7FfORHXIA";
  $test_auth = ":test_02IkoZ-uq1_vs-MW3LrAgg";


  $url = "https://api.scalablepress.com/v2/".$ep;
  $headers = array(
    'Content-type: application/json'
  );

  $data = json_encode($data);

  $ch = curl_init();
  $options = [
    CURLOPT_URL => $url,
    CURLOPT_USERPWD => $auth,
    CURLOPT_HTTPHEADER => $headers,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_POSTFIELDS => $data
  ];
  curl_setopt_array($ch, $options);

  $result = curl_exec($ch);
  curl_close($ch);
  return json_decode($result, true);
}

function addProduct($data){
  $ep = "products";
  $res = wordpressRequest($ep, $data);

  makeVariations($res['id']);

  return $res['permalink'];
}


function makeVariations($id){
  $ep = "products/$id/variations/batch";

  $data = [
    'create' => [
      [
        'regular_price' => '25.00',
        'attributes' => [
          [
            'name' => 'Size',
            'option' => 'S'
          ]
        ]
      ],
      [
        'regular_price' => '25.00',
        'attributes' => [
          [
            'name' => 'Size',
            'option' => 'M'
          ]
        ]
      ],
      [
        'regular_price' => '25.00',
        'attributes' => [
          [
            'name' => 'Size',
            'option' => 'L'
          ]
        ]
      ]
    ]
  ];

  $res = wordpressRequest($ep, $data);
}

function colorWanderItem($seed){
  $img = "https://thealgos.design/algorithm/img/color_wander/".$seed.".png";
  $data = [
    'name' => 'Color Wander '.$seed,
    'type' => 'variable',
    'description' => '',
    'short_description' => '',
    'regular_price' => '25.00',
    'categories' => [
      [
        'id' => 15
      ]
    ],
    'images' => [
      [
        'src' => $img,
        'position' => 0
      ]
    ],
    'attributes' => [
      [
        'name' => 'Size',
        'position' => 0,
        'visible' => false,
        'variation' => true,
        'options' => [
          'S',
          'M',
          'L'
        ]
      ],
      [
        'name' => 'Color',
        'position' => 0,
        'visible' => true,
        'variation' => true,
      ],
      [
        'name' => 'Type',
        'position' => 0,
        'visible' => false,
        'variation' => false,
      ]
    ],
    'default_attributes' => [
      [
        'name' => 'Size',
        'option' => 'S'
      ],
      [
        'name' => 'Type',
        'option' => 'dtg'
      ]

    ]
  ];

  return $data;
}

function wordpressRequest($ep, $data){
  $key = "ck_5036c204f64edcd0bd9ddf59331d4ce089daadfd";
  $secret = "cs_cc8a6e5e80c98148bc28579c7c5aff6c53d08be5";
  $auth = $key.':'.$secret;

  $url = "https://thealgos.design/wp-json/wc/v2/".$ep;
  $headers = array(
    'Content-type: application/json'
  );

  $data = json_encode($data);

  $ch = curl_init();
  $options = [
    CURLOPT_URL => $url,
    CURLOPT_USERPWD => $auth,
    CURLOPT_HTTPHEADER => $headers,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_POSTFIELDS => $data
  ];
  curl_setopt_array($ch, $options);

  $result = curl_exec($ch);
  curl_close($ch);
  return json_decode($result, true);
}

?>