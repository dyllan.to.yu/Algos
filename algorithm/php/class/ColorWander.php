<?php
require_once(__DIR__.'/../imager.php');
require_once(__DIR__.'/../ScalablePress.php');
require_once(__DIR__.'/../WooCommerce.php');

class ColorWander{
	var $seed;
	var $original;
	var $transparent;
	var $bgColor;
	var $designID;
	var $wpID;
	var $permalink;

	function __construct($imgB64, $seed, $dir){
		$this->seed = $seed;
		$img = saveImage($imgB64, $seed, $dir);
		$this->original = $img['original'];
		$this->transparent = $img['transparent'];
		$this->bgColor = $img['color'];
	}
	function create(){
		$this->designID = getDesignID($this->transparent);
		$woo = new WooCommerce();
		$res = $woo->createProduct($this->productData());
		$var = $woo->createVariations($res->id, $this->variationData());
		$this->permalink = $res->permalink;
	}

	function productData(){
		$img = "https://thealgos.design/algorithm/" . $this->original;
	  $data = [
	    'name' => 'Color Wander '. $this->seed,
	    'type' => 'variable',
	    'description' => '',
	    'short_description' => '',
	    'regular_price' => '25.00',
	    'categories' => [
	      [
	        'id' => 15
	      ]
	    ],
	    'images' => [
	      [
	        'src' => $img,
	        'position' => 0
	      ]
	    ],
	    'attributes' => [
	      [
	      	'id' => 1,
	        'name' => 'Size',
	        'position' => 0,
	        'visible' => false,
	        'variation' => true,
	        'options' => [
	          'S',
	          'M',
	          'L'
	        ]
	      ]
	    ],
	    'default_attributes' => [
	      [
	        'id' => 1,
	        'option' => 'S'
	      ]
	    ],
	    'meta_data' => [
	    	[
	    		'key' => 'designID',
	    		'value' => $this->designID
	    	],
	    	[
	    		'key' => 'hexcolor',
	    		'value' => $this->bgColor
	    	],
	    	[
	    		'key' => 'printtype',
	    		'value' => 'dtg'
	    	]
	    ]
	  ];
	  return $data;
	}

	function variationData(){
		$data = [
	    'create' => [
	      [
	        'regular_price' => '25.00',
	        'attributes' => [
	          [
	          	'id' => 1,
	            'name' => 'Size',
	            'option' => 'S'
	          ]
	        ]
	      ],
	      [
	        'regular_price' => '25.00',
	        'attributes' => [
	          [
	          	'id' => 1,
	            'name' => 'Size',
	            'option' => 'M'
	          ]
	        ]
	      ],
	      [
	        'regular_price' => '25.00',
	        'attributes' => [
	          [
	          	'id' => 1,
	            'name' => 'Size',
	            'option' => 'L'
	          ]
	        ]
	      ]
	    ]
	  ];
	  return $data;
	}
}