//============================================================
// ZALGO text script by tchouky
//============================================================

// data set of leet unicode chars
//---------------------------------------------------

var zalgo = require('./chars.json');


// rand funcs
//---------------------------------------------------

//gets an int between 0 and max
function rand(max)
{
	return Math.floor(Math.random() * max);
}

//gets a random char from a zalgo char table
function rand_zalgo(array)
{
	var ind = Math.floor(Math.random() * array.length);
	return array[ind];
}

//lookup char to know if its a zalgo char or not
function is_zalgo_char(c)
{
	for(let i=0; i<zalgo.up.length; i++)
		if(c == zalgo.up[i])
			return true;
	for(let i=0; i<zalgo.down.length; i++)
		if(c == zalgo.down[i])
			return true;
	for(let i=0; i<zalgo.mid.length; i++)
		if(c == zalgo.mid[i])
			return true;
	return false;
}

function zalgoify(text, size = 'm', directions = ['up','down']){
	let res = '';
	
	if(!Number.isInteger(size)){
		let sizes = {
			's': 8,
			'm': 16,
			'l': 32,
			'xl':64
		};
		size = sizes[size];
		if (!Number.isInteger(size) || size === undefined) {
			throw "Invalid Size";
		}
	}
	let dense = 1;
	if(size >= 16)
		dense = Math.sqrt(size) / 2;

	for(let i = 0; i < text.length; i++){
		if(is_zalgo_char(text.substr(i, 1)))
			continue;

		res += text.substr(i, 1);

		let num = {
			'up': rand(size) / dense,
			'med': rand(size/4) / dense,
			'down': rand(size) / dense
		};

		if(directions.includes('up'))
			for(let j=0; j<num.up; j++)
				res += rand_zalgo(zalgo.up);
		if(directions.includes('mid'))
			for(let j=0; j<num.mid; j++)
				res += rand_zalgo(zalgo.mid);
		if(directions.includes('down'))
			for(let j=0; j<num.down; j++)
				res += rand_zalgo(zalgo.down);
	}
	return res;
}

function zalgo_step(text, size = 'm', directions = ['up','down']){
	let res = [];
	
	if(!Number.isInteger(size)){
		let sizes = {
			's': 8,
			'm': 16,
			'l': 32,
			'xl':64
		};
		size = sizes[size];
		if (!Number.isInteger(size) || size === undefined) {
			throw "Invalid Size";
		}
	} 
	let dense = 1;
	if(size >= 16)
		dense = Math.sqrt(size) / 2;

	let max = size/dense;
	
	for (let i = 0; i < max; i++) {
		res[i] = '';
	}

	for(let i = 0; i < text.length; i++){
		if(is_zalgo_char(text.substr(i, 1)))
			continue;
		
		let letter = text.substr(i, 1);

		let num = {
			'up': rand(size) / dense,
			'med': rand(size/4) / dense,
			'down': rand(size) / dense
		};
		for(let j = 0; j < max; j++){
			if(directions.includes('up') && j < num.up){
				letter += rand_zalgo(zalgo.up);
			}
			if(directions.includes('mid') && j < num.mid){
				letter += rand_zalgo(zalgo.mid);
			}
			if(directions.includes('down') && j < num.down){
				letter += rand_zalgo(zalgo.down);
			}

			res[j] += letter;
		}
	}
	return res;
}
let z = zalgo_step("Algos")
console.log(z);