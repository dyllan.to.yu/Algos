<?php 
/*
Plugin Name:  Scalable Press
Description:  Scalable Press Integration
Version:      0.1
Author:       Dyllan To
Author URI:   https://dyllan.to
*/
require_once('ScalablePressOrder.php');

if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
	// include 'options_menu.php';
	add_action('woocommerce_payment_complete', 'scalable_press_order');
}

function scalable_press_order($order_id){
	$order = wc_get_order( $order_id );
	
	$a = $order->get_address();
	$address = ScalablePressOrder::wc_newAddress($a);
	$SP = new ScalablePressOrder($address);

	$items = $order->get_items();
	foreach($items as $item){
		$product = $item->get_product();
		if($product){
			$parentid = $product->get_parent_id();
			
			$hexcolor = get_post_meta($parentid, 'hexcolor', true);
			$quantity = $item->get_quantity();
			$size = $product->get_attribute('size');
			
			$printtype = get_post_meta($parentid, 'printtype', true);
			$designid = get_post_meta($parentid, 'designID', true);
			$prod = $ScalablePressOrder::newProduct($productID, $hexcolor, $quantity, $size);

			$SP->addItem($printtype, $designid, $prod);
		}
	}
	echo $SP->order();
}