<?php
add_action('admin_menu', 'scalable_press_menu');
function scalable_press_menu(){
	add_options_page('Scalable Press Options', 'Scalable Press', 'manage_options', 'scalable_press', 'sp_options_page');
}

function sp_options_page() {
	if(!current_user_can('manage_options')) {
		wp_die(__('You do not have sufficient permissions to access this page.'));
	}
	?>
	<div class="wrap">
		<h1>Scalable Press API</h1>
		<form method="post" action="options.php">';
			<?php settings_fields('sp_options'); ?>
			<?php do_settings_sections('sp_plugin')); ?>
		</form>
	</div>
<?php
} 

add_action('admin_init', 'plugin_admin_init');
function plugin_admin_init(){
	register_setting('sp_options', 'sp_options', 'sp_validate');
	add_settings_section('sp_main', 'Scalable Press Settings', 'sp_section_text', 'sp_plugin');
	add_settings_field('sp_key', 'Scalable Press API Key', 'sp_setting_string', 'sp_plugin', 'sp_main');
}

function sp_section_text() {
	echo '<p>Scalable Press Description stuff</p>';
}

function sp_setting_string() {
	$options = get_option('sp_options');
	echo "<input id='sp_key' name='sp_options[key]' size='40' type='text' value='{$options['key']}' />";
}

 // validate our options
function sp_validate($input) {
$newinput['text_string'] = trim($input['text_string']);
if(!preg_match('/^[a-z0-9]{32}$/i', $newinput['text_string'])) {
$newinput['text_string'] = '';
}
return $newinput;
}
