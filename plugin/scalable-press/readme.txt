=== Scalable Press ===
Contributors: jarwain
Requires at least: 4.9.4
Tested up to: 4.9.4
Requires PHP: 7.0.27
License: GPLv3
License URI: http://www.gnu.org/licenses/gpl-3.0.html

Scalable Press Plugin