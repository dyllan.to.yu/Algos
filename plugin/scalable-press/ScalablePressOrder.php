<?php

class ScalablePressOrder {
	var $items;
	var $address;

	function __construct($address = null, $item = []){
		$this->address = $address;
	}

	function order(){
		return scalablePressRequest("quote/bulk", $this->items);
	}

	function addItem($type, $designId, $products){
		$i = [
			'type' => $type,
			'designId' => $designId,
			'products' => [$product],
			'address' => $this->address
		];
		$this->items[] = $i;
	}

	static function newProduct($id, $color, $quantity, $size){
		$p = [
			"id" => $id,
			"color" => $color,
			"quantity" => $quantity,
			"size" => $size
		];
		return $p;
	}

	static function wc_newAddress($a){
		$addr = [];
		$name = "$a[first_name] $a[last_name]";
		if(isset($name)){
			$addr['name'] = $name;
		}
		if(isset($a['company'])){
			$addr['company'] = $company;
		}
		if(isset($a['address_1'])){
			$addr['address1'] = $address_1;
		}
		if(isset($a['address_2'])){
			$addr['address2'] = $address_2;
		}
		if(isset($a['city'])){
			$addr['city'] = $city;
		}
		if(isset($a['state'])){
			$addr['state'] = $state;
		}
		if(isset($a['zip'])){
			$addr['zip'] = $zip;
		}
		if(isset($a['country'])){
			$addr['country'] = $country;
		}
		return $addr;
	}

	static function newAddress($name, $company = null, $address1, $address2 = null, $city, $state, $zip, $country = null){
		$addr = [];
		if(isset($name)){
			$addr['name'] = $name;
		}
		if(isset($company)){
			$addr['company'] = $company;
		}
		if(isset($address1)){
			$addr['address1'] = $address1;
		}
		if(isset($address2)){
			$addr['address2'] = $address2;
		}
		if(isset($city)){
			$addr['city'] = $city;
		}
		if(isset($state)){
			$addr['state'] = $state;
		}
		if(isset($zip)){
			$addr['zip'] = $zip;
		}
		if(isset($country)){
			$addr['country'] = $country;
		}
		return $addr;
	}

	function scalablePressRequest($ep, $data, $test = true){
	  $auth = ":live_hPh3gPG4HheYA7FfORHXIA";
	  $test_auth = ":test_02IkoZ-uq1_vs-MW3LrAgg";

	  $url = "https://api.scalablepress.com/v2/".$ep;
	  $headers = array(
	    'Content-type: application/json'
	  );

	  $data = json_encode($data);

	  $ch = curl_init();
	  $options = [
	    CURLOPT_URL => $url,
	    CURLOPT_USERPWD => $test ? $test_auth : $auth,
	    CURLOPT_HTTPHEADER => $headers,
	    CURLOPT_RETURNTRANSFER => true,
	    CURLOPT_POSTFIELDS => $data
	  ];
	  curl_setopt_array($ch, $options);

	  $result = curl_exec($ch);
	  curl_close($ch);
	  return json_decode($result, true);
	}
}