ALTERNATE TLDs:
    algos.ink           $6.88/year      Retail $24.88/year
        Main Site
    algos.wtf           $3.88/year      Retail $24.88/year
        Testing/Staging

PROCEDURALLY GENERATED GAME-WORLDS
    Partner with legit companies like
    - Terraria
        - https://github.com/TerraMap/terramap.github.io
        - Create a Scene in Terraria with all the Devs
    - Starbound?
        - https://playstarbound.com/
    Biggest allowable split is 50%. Offer 10% and the option to double if aid is rendered.
    Create a product THEN send it to them, just to gauge their interest

User Creates/Finds Image, Presses Button
Product & Variations are Generated & Added to Woocommerce
User is brought to newly made product page
---
On Order Creation && Payment
Submit Order to relevant printer

Colorwander Submission
{
	type: "color-wander"
	seed: INT
	imgBase64: ""
}

WeaveSilk Submission
{
	type: "WeaveSilk"
	imgBase64: ""
}

Zalgo Submission
{
	type: "Zalgo"
	...?
}


# T-Shirt Brands
## Unisex/Men

Anvil 980 |||   8.53
    Lightweight / Soft Touch / Relaxed / Fitted / Skate/ Street Wear / Cross fit T-shirt
    Alt to American Apparel 2001
    pre-shrunk
    56 colors
    perfect flush side prints
    4.5oz
Anvil 780
    Same but midweight
    fewer colors
    5.4oz

Next Level 3600 |   9.41
    100% ringspun
    31 colors (meh)
    4.3oz

Bella Canvas 3001 ||| 9.29
    Soft Touch / Fitted / Comfort / Casual / Street Wear/ Cross fit T-shirt/ Vintage Style
    100% combed ringspin
    57 colors
    4.2
    3001U = USA
    3001C = Standard

## WOMEN
Bella Canvas 6004